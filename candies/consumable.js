"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const generalCtrl_1 = require("../generalCtrl");
/**
 * Indicates how params should be interpreted and used in GenCtrl
 * e.g. params = [{request_id: "_id", name: "fname"}]
 * will return a request with req.params.baloPatch = {"_id": "1234", "fname": "Amin"}
 * @param params
 * @param req
 */
function paramsHandler(params, req) {
    return __awaiter(this, void 0, void 0, function* () {
        let newParams = {};
        if (params) {
            params.forEach(element => {
                let key = Object.keys(element)[0];
                let val = element[key];
                newParams = Object.assign({}, newParams, { [val]: req.params[key] });
            });
        }
        req.params.baloPatch = newParams;
        return req;
    });
}
function ioParamsHandler(ioParams, req) {
    return __awaiter(this, void 0, void 0, function* () {
        if (ioParams) {
            req.params.IOEvent = ioParams.IOEvent;
            req.params.IOContent = ioParams.IOContent;
        }
        return req;
    });
}
/**
 * Prepares the population data for GenCtrl
 * @param populationOptions
 * @param req
 */
function prepForPopulation(populationOptions, req) {
    return __awaiter(this, void 0, void 0, function* () {
        req.params.run_population = true;
        req.params.population_path = populationOptions.path;
        req.params.population_fields = populationOptions.select;
        return req;
    });
}
/**
 * A Decorator created give CRUD capabilities to decorated methods
 * @param model the database model
 * @param config configuration to indicate which request to use and how
 */
function consume(model, config = { request: "read-all" }) {
    return function (target, propertyName, propertyDescriptor) {
        const method = propertyDescriptor.value;
        propertyDescriptor.value = function (...args) {
            return __awaiter(this, void 0, void 0, function* () {
                let result; // Result of database operation
                let instance = new generalCtrl_1.GenCtrl(model); // Instance of GenCtrl
                // Patch params correctly and reassign
                args[0] = yield paramsHandler(config.params, args[0]);
                // Patch IO event
                args[0] = yield ioParamsHandler(config.ioParams, args[0]);
                if (config.populationOptions) {
                    args[0] = yield prepForPopulation(config.populationOptions, args[0]);
                }
                switch (config.request) {
                    case "read-all":
                        result = yield instance.readDataByIdV2(args[0]);
                        break;
                    case "read-single":
                        result = yield instance.readSingleItemV2({ req: args[0] });
                        break;
                    case "search":
                        result = yield instance.searchDataV2({ req: args[0], _res: args[1] });
                        break;
                    case "add-data":
                        result = yield instance.addDataV2(args[0], args[1]);
                        break;
                    case "update-data-by-id":
                        result = yield instance.updateDataByIdV2(args[0], args[1]);
                        break;
                    case "delete-data":
                        result = yield instance.deleteDataByIdV2(args[0], args[1]);
                        break;
                    default:
                        result = { message: "No Request type indicated", error: true };
                        break;
                }
                config.autoRespond !== false ?
                    args[1].send(result) :
                    args[2] = result;
                return method.apply(this, args);
            });
        };
        return propertyDescriptor;
    };
}
exports.consume = consume;
//# sourceMappingURL=consumable.js.map