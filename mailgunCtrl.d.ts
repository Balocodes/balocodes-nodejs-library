/**
 * Mailgun Controller
*/
declare class MailCtrl {
    content_type: string;
    sender: string;
    receiver: string;
    subject: string;
    content: string;
    response: any;
    /**
     *
     * @param {String} content_type ("text" or "html")
     * @param {String} sender (Email of sender)
     * @param {String} receiver (Email of receiver)
     * @param {String} subject (Subject of email)
     * @param {String} content (The content of the email)
     * @param {Object} response (Express response)
     */
    constructor(content_type: string, sender: string, receiver: string, subject: string, content: string, response?: null);
    /**
     * Triggers the send mail method
     */
    send(): void;
}
export declare const MailController: typeof MailCtrl;
export {};
