"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
// Created by @balocodes
const log_1 = require("./models/log/log");
const errorLog_1 = require("./models/log/errorLog");
const misc_1 = require("./misc");
class GeneralUtility {
    constructor(model, response, request, sendResp) {
        this.dataCleaner = misc_1.dataCleaner;
        this.afterRequestCompletedHasError = false;
        this.ioContent = {};
        this.defaultMessages = {
            successMessage: "Operation successful",
            errorMessage: "OperationFailed"
        };
        /**
        * A callback ran after request is completed.
        * @param errFunc
        * @param successFunc
        * @param isError
        */
        this.afterRequestCallback = (hasError) => {
            if (hasError) {
                this.errorHandler();
            }
            else {
                this.successHandler();
            }
        };
        this.model = model;
        this.response = response;
        this.request = request;
        this.sendResp = sendResp;
    }
    static logger(who, action, model_name = "", previous_data = null) {
        log_1.logModel.create([
            {
                who: who,
                action: action,
                model_name: model_name,
                previous_data: previous_data
            }
        ]);
    }
    static errorLogger(error) {
        errorLog_1.errorLogModel.create([{ error: error }]);
    }
    defaultOptions() {
        return {
            sendResp: true
        };
    }
    /**
     * Works exactly like console.log
     * The only difference is that it only logs data when NODE_ENV=development
     * Otherwise, it does nothing.
     * @param param1
     * @param param2
     */
    static printData(...params) {
        if (process.env.NODE_ENV === "development") {
            console.log(params);
        }
        else {
            console.log("NODE_ENV", "Cannot Log Data in production mode");
        }
    }
    /**
     * Decides whether or not response is sent to client and what format
     * @param err
     * @param data
     * @param code
     */
    defaultCallback(err, data, code = 200) {
        return __awaiter(this, void 0, void 0, function* () {
            let errStatusCode = 500;
            if (err) {
                err.code === null
                    ? GeneralUtility.printData("Error", err)
                    : GeneralUtility.printData("Error", err.code);
                switch (err.code) {
                    case 11000:
                        GeneralUtility.printData("Error", err);
                        errStatusCode = 409;
                        err.message = "Data already exists!";
                        if (this.request.logger) {
                            GeneralUtility.logger(this.request.decoded._id, `Duplicate error: ${err.code}`, this.model.modelName);
                        }
                        break;
                    case "NO_ID":
                        GeneralUtility.printData(err);
                        errStatusCode = 404;
                        err.message = "Could not identify data.";
                        if (this.request.logger) {
                            GeneralUtility.logger(this.request.decoded._id, `Tried to update without ID: ${err.code}`, this.model.modelName);
                        }
                        break;
                    default:
                        err.message = "Operation failed!";
                        if (this.request.logger) {
                            GeneralUtility.logger(this.request.decoded._id, `Unknown error: ${err.code}`, this.model.modelName);
                        }
                        break;
                }
                if (this.sendResp) {
                    GeneralUtility.printData("Error", err);
                    this.response.status(errStatusCode).send({
                        error: true,
                        message: err.message,
                        code: err.code
                    });
                    this.response.end();
                }
                if (this.request.logger) {
                    GeneralUtility.logger(this.request.decoded._id, `Operation failed: ${err.code}`, this.model.modelName);
                }
            }
            else if (data) {
                if (this.sendResp) {
                    this.response.status(code).send({
                        error: false,
                        result: yield misc_1.filterObject(data, data.fieldsToRemove),
                        message: "Operation Successful!"
                    });
                    this.response.end();
                }
                if (this.request.logger) {
                    GeneralUtility.logger(this.request.decoded._id, "Updated/Added data successfully!", this.model.modelName);
                }
            }
            else {
                if (this.sendResp) {
                    this.response.status(code).send({
                        error: false,
                        result: data,
                        message: "Operation did not throw any error but no response either"
                    });
                    this.response.end();
                }
                if (this.request.logger) {
                    GeneralUtility.logger(this.request.decoded._id, "Generic Response", this.model.modelName);
                }
            }
        });
    }
    errorHandler() {
        //pass
    }
    successHandler() {
        //pass
        if (this.io) {
            this.io.emit(this.ioEvent, this.ioContent);
            this.ioEvent = null;
        }
    }
    static successHandler({ io, event, content = "" }) {
        if (event) {
            io.emit(event, content);
        }
    }
    /**
     * Returns response code
     * @param err
     * @param code
     */
    respCode(err = null, code = 200, callback = this.afterRequestCallback) {
        if (err) {
            this.afterRequestCompletedHasError = true;
            callback(true);
            return 500;
        }
        this.afterRequestCompletedHasError = false;
        callback(false);
        return code;
    }
}
exports.GeneralUtility = GeneralUtility;
//# sourceMappingURL=genUtitlity.js.map