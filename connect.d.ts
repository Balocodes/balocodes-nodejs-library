interface MssqlConfig {
    user: string;
    password: string;
    server: string;
    database: string;
    options: {
        encrypt: boolean;
    };
}
export declare const mongooseFunc: (link: string) => Promise<void>;
export declare const mssqlFunc: (config: MssqlConfig) => Promise<void>;
export {};
