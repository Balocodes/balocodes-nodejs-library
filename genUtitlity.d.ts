import { Model, Document } from "mongoose";
import { Socket } from "socket.io";
export declare class GeneralUtility {
    static io: Socket;
    model: Model<Document>;
    response: any;
    request: any;
    sendResp: any;
    dataCleaner: (obj: any) => {
        data: any;
        limit: number;
        page: number;
        order: any;
        fields: any;
        range: any;
        find: any;
        search: any;
        pathOfModelToPopulate: string;
        validator: (x: any[]) => boolean;
    };
    afterRequestCompletedHasError: boolean;
    io: any;
    ioEvent: any;
    ioContent: {};
    defaultMessages: {
        successMessage: string;
        errorMessage: string;
    };
    constructor(model: Model<Document>, response?: any, request?: any, sendResp?: any);
    static logger(who: any, action: string, model_name?: string, previous_data?: Document | null): void;
    static errorLogger(error: string): void;
    defaultOptions(): {
        sendResp: boolean;
    };
    /**
     * Works exactly like console.log
     * The only difference is that it only logs data when NODE_ENV=development
     * Otherwise, it does nothing.
     * @param param1
     * @param param2
     */
    static printData(...params: any): void;
    /**
     * Decides whether or not response is sent to client and what format
     * @param err
     * @param data
     * @param code
     */
    defaultCallback(err: any, data: any, code?: number): Promise<void>;
    errorHandler(): void;
    successHandler(): void;
    static successHandler({ io, event, content }: {
        io: Socket;
        event: string;
        content: any;
    }): void;
    /**
    * A callback ran after request is completed.
    * @param errFunc
    * @param successFunc
    * @param isError
    */
    afterRequestCallback: (hasError: boolean) => void;
    /**
     * Returns response code
     * @param err
     * @param code
     */
    respCode(err?: null, code?: number, callback?: (hasError: boolean) => void): number;
}
