"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
// Created by @balocodes
// import { connect, Mongoose } from "mongoose";
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
const config_1 = require("../config");
const fs = require("fs");
const csv = require("csvtojson");
const formidable = require("formidable");
/**
 * This handles stringified data and filters out
 * unneccessary fields
 * @param {Object} shell
 */
exports.dataCleaner = function dataCleaner(obj) {
    let shell = {
        data: obj.data || {},
        limit: Number(obj.limit),
        page: Number(obj.page),
        order: obj.order,
        fields: obj.fields,
        range: obj.range,
        find: obj.find,
        search: obj.search,
        pathOfModelToPopulate: "",
        validator: (x) => {
            let validated = true;
            for (let i in x) {
                if (x[i] === "" || x[i] === null || x[i] === undefined) {
                    validated = false;
                }
            }
            return validated;
        }
    };
    if (typeof shell.data === "string") {
        shell.data = JSON.parse(shell.data);
    }
    if (typeof shell.find === "string") {
        shell.find = JSON.parse(shell.find);
    }
    if (!shell.limit) {
        shell.limit = 10;
    }
    if (!shell.page) {
        shell.page = 0;
    }
    if (!shell.order) {
        shell.order = "-_id";
    }
    if (shell.range) {
        if (typeof shell.range === "string") {
            shell.range = JSON.parse(shell.range);
        }
        for (let x = 0; x < shell.range.length; x++) {
            if (shell.range[x].field && shell.range[x].gte && shell.range[x].lte) {
                shell.data[shell.range[x].field] = {
                    $gte: shell.range[x].gte,
                    $lte: shell.range[x].lte
                };
            }
            else if (shell.range[x].field && shell.range[x].gte) {
                shell.data[shell.range[x].field] = {
                    $gte: shell.range[x].gte
                };
            }
            else if (shell.range[x].field && shell.range[x].lte) {
                shell.data[shell.range[x].field] = {
                    $lte: shell.range[x].lte
                };
            }
        }
    }
    return shell;
};
/**
 * This function takes a string containing properties of an object
 * as well as the object and removes those properties from the
 * object
 * @param {Object} obj
 * @param {String} toRemove
 */
exports.filterObject = function filterObject(obj, toRemove) {
    if (!toRemove) {
        return obj;
    }
    let counter = 0;
    let removeArr = toRemove.split(" ");
    for (let x in obj) {
        if (removeArr.includes(x)) {
            delete obj[x];
        }
    }
    return obj;
};
exports.tokenVerifier = (req, res, next, secret) => {
    let token = req.body.token || req.query.token;
    try {
        req.decoded = jsonwebtoken_1.default.verify(token, String(secret));
        next();
    }
    catch (error) {
        return res.send({
            message: "Verification failed. Please log in again.",
            code: 401,
            error: true
        });
    }
};
function fileUploader(req, res, filePath = config_1.defaultUploadLocation) {
    return (req, res) => {
        const form = new formidable.IncomingForm();
        form.parse(req);
        form.on('fileBegin', (name, file) => {
            if (req.query._id) {
                const dir = `/${filePath}/${req.query._id}/`;
                file.path = `${dir}${file.name}`;
                if (!fs.existsSync(dir)) {
                    fs.mkdirSync(dir, `0744`);
                }
            }
            else {
                file.path = `/${filePath}/` + file.name;
            }
        }).on('file', (name, file) => {
            console.log('Uploaded ' + file.name);
            res.send({ message: `${file.path}`, error: false });
            res.end();
        }).on('aborted', () => {
            console.error('Request aborted by the user');
            res.send({ message: "Request aborted!", error: true });
            res.end();
        }).on('error', (err) => {
            console.error('Error', err);
            res.send({ message: "An error occured!", error: true });
            res.end();
            throw err;
        }).on('end', (data) => {
            res.send(data);
            res.end();
        });
    };
}
exports.fileUploader = fileUploader;
function fileUploaderHandler(req, res) {
    console.log("Trying to upload file");
}
exports.ioEmitterSelector = (dept) => {
    let emitterMap = config_1.emitterDataMap;
    return emitterMap[dept];
};
//# sourceMappingURL=misc.js.map